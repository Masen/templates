# source files directory, i.e. '.c' or '.cpp' et cetera
SRC_DIR=src
# object files directory
LIB_DIR=lib
# header files directory
INCLUDE_DIR=include

# sets the compiler to be used
CC=tcc

# currently only links math.h
LIBS=-lm

# sets the output binary
BIN=bin
# finds all header files located in the include directory
HEADERS=$(shell find $(INCLUDE_DIR) -name '*.h')
# finds all source files located in the src directory
SRC_FILES=$(shell find $(SRC_DIR) -name '*.c')
# finds all object files located in the lib directory
OBJECTS=$(patsubst $(SRC_DIR)/%.c, $(LIB_DIR)/%.o, $(SRC_FILES))

# tells make that the target (all) isn't a file
.PHONY: all
# default rule
all: $(BIN)

# compiles source files to object files
$(LIB_DIR)/%.o: $(SRC_DIR)/%.c $(HEADERS)
	mkdir -p $(dir $@)
	$(CC) -o $@ -c $<

# builds binary from object files
$(BIN): $(OBJECTS)
	$(CC) $(LIBS) -o $@ $^

# tells make that the target (clean) isn't a file
.PHONY: clean
# rids the working directory of all compiled object files and binaries
clean:
	rm -rf $(LIB_DIR) $(BIN)
